//
//  ViewController.swift
//  Notes
//
//  Created by Rostislav Aliferovich on 7/4/19.
//

import UIKit
import CocoaLumberjack

class ViewController: UIViewController {
  override func viewDidLoad() {
    DDLogVerbose("Main View Controller loaded")
    super.viewDidLoad()
  }

  override func viewWillAppear(_ animated: Bool) {
    DDLogVerbose("Main View Controller will appear soon")
    super.viewWillAppear(animated)
  }

  override func viewDidAppear(_ animated: Bool) {
    DDLogVerbose("Main View Controller appeared")
    super.viewWillAppear(animated)
  }

  override func viewWillLayoutSubviews() {
    DDLogVerbose("Main View Controller will update subviews's layout")
    super.viewWillLayoutSubviews()
  }

  override func viewWillDisappear(_ animated: Bool) {
    DDLogVerbose("Main View Controller will disappear soon")
    super.viewWillDisappear(animated)
  }

  override func viewDidDisappear(_ animated: Bool) {
    DDLogVerbose("Main View Controller disappeared")
    super.viewDidDisappear(animated)
  }
}
