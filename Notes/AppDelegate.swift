//
//  AppDelegate.swift
//  Notes
//
//  Created by Rostislav Aliferovich on 7/4/19.
//

import UIKit
import CocoaLumberjack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    configureLogger()
    checkConfiguration()
    return true
  }

  private func configureLogger() {
    #if DEBUG
    dynamicLogLevel = .all
    #else
    dynamicLogLevel = .warning
    #endif
    DDLog.add(DDOSLogger.sharedInstance)
    let fileLogger = DDFileLogger()
    fileLogger.rollingFrequency = 60 * 60 * 24
    fileLogger.logFileManager.maximumNumberOfLogFiles = 7
    DDLog.add(fileLogger)

    DDLogVerbose("Verbose")
    DDLogInfo("Debug")
    DDLogInfo("Info")
    DDLogWarn("Warn")
    DDLogError("Error")
  }

  private func checkConfiguration() {
    DDLogInfo("Check Application Configuration")
    do {
      let baseUrl = try Configuration().baseUrl
      DDLogInfo("Base URL = \(baseUrl)")
    } catch ConfigurationError.missingInfoPlist {
      let message = ConfigurationError.missingInfoPlist.description
      DDLogError(message)
      fatalError(message)
    } catch ConfigurationError.variableNotFound(let variable) {
      let message = ConfigurationError.variableNotFound(variable).description
      DDLogError(message)
      fatalError(message)
    } catch {
      let message = "Unknown error"
      DDLogError(message)
      fatalError(message)
    }
  }
}
