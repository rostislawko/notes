import UIKit

extension Note: Codable {
  enum CodingKeys: String, CodingKey {
    case uid
    case title
    case content
    case color
    case importance
    case selfDestructionDate
  }
  
  static func parse(json: [String: Any]) -> Note? {
    guard let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []),
          let note = try? JSONDecoder().decode(Note.self, from: jsonData) else { return nil }
    return note
  }
  
  var json: [String: Any] {
    guard let jsonData = try? JSONEncoder().encode(self),
          let json = try? JSONSerialization.jsonObject(with: jsonData, options: []) else {
      return [:]
    }
    return json as? [String: Any] ?? [:]
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    uid = try container.decode(String.self, forKey: .uid)
    title = try container.decode(String.self, forKey: .title)
    content = try container.decode(String.self, forKey: .content)
    if let colorRepresentation = try container.decode(String?.self, forKey: .color) {
      color = UIColor(stringRepresentation: colorRepresentation)
    } else {
      color = .white
    }
    let importanceRepresentation = try container.decode(Int?.self, forKey: .importance) ??
      Importance.medium.rawValue
    importance = Importance(rawValue: importanceRepresentation) ?? .medium
    if let selfDestructionDateRepresentation = try container.decode(
      String?.self,
      forKey: .selfDestructionDate
    ) {
      selfDestructionDate = DateFormatter.default.date(from: selfDestructionDateRepresentation)
    } else {
      selfDestructionDate = nil
    }
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(uid, forKey: .uid)
    try container.encode(title, forKey: .title)
    try container.encode(content, forKey: .content)
    if color != .white {
      try container.encode(color.stringRepresentation, forKey: .color)
    }
    if importance != .medium {
      try container.encode(importance.rawValue, forKey: .importance)
    }
    if let date = selfDestructionDate {
      let selfDestructionDateRepresentation = DateFormatter.default.string(from: date)
      try container.encode(selfDestructionDateRepresentation, forKey: .selfDestructionDate)
    }
  }
}
