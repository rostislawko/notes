import Foundation

enum FileError: Error {
  case read
  case write
  case createDirectory
  case unknown

  var description: String {
    switch self {
    case .read: return "Could not read notes"
    case .write: return "Could not save notes"
    case .createDirectory: return "Can't create directory"
    case .unknown: return "Unknown error"
    }
  }
}

class FileNotebook {
  private(set) var notes: [Note] = []
  private var fileManager = FileManager.default

  public func add(_ note: Note) {
    guard notes.first(where: { $0.uid == note.uid }) == nil else { return }
    notes.append(note)
  }

  public func remove(with uid: String) {
    guard let index = notes.firstIndex(where: { $0.uid == uid }) else { return }
    notes.remove(at: index)
  }

  // For unit testing this Encoding code and FileManager should be stubbed
  public func saveToFile() throws {
    do {
      let notesData = try JSONEncoder().encode(notes)
      try notesData.write(to: notesDirectory())
    } catch {
      throw FileError.write
    }
  }

  // For unit testing this Decoding code and FileManager should be stubbed
  public func loadFromFile() throws {
    do {
      let notesData = try Data(contentsOf: notesDirectory())
      notes = try JSONDecoder().decode([Note].self, from: notesData)
    } catch {
      throw FileError.write
    }
  }

  // For unit testing this FileManager should be stubbed
  private func notesDirectory() throws -> URL {
    guard let cachesDirectory = fileManager.urls(
      for: .cachesDirectory,
      in: .userDomainMask
    ).first else { throw FileError.unknown }
    let notesDirectory = cachesDirectory.appendingPathComponent("NoteBook")
    var isDirectory: ObjCBool = false
    var shouldCreateDirectory = false
    var shouldRemoveExistingFile = false
    if fileManager.fileExists(atPath: notesDirectory.absoluteString, isDirectory: &isDirectory) {
      if isDirectory.boolValue == false {
        shouldRemoveExistingFile = true
        shouldCreateDirectory = true
      }
    } else {
      shouldCreateDirectory = true
    }
    do {
      if shouldRemoveExistingFile { try fileManager.removeItem(at: notesDirectory) }
      if shouldCreateDirectory {
        try fileManager.createDirectory(at: notesDirectory, withIntermediateDirectories: true)
      }
    } catch {
      throw FileError.createDirectory
    }
    return notesDirectory
  }
}
