//
//  PlistReader.swift
//  Notes
//
//  Created by Rostislav Aliferovich on 7/5/19.
//

import Foundation

enum PlistReaderError: Error {
  case read(String)

  var description: String {
    switch self {
    case .read(let plistName): return "Could not read \(plistName).plist"
    }
  }
}

protocol PlistReader {
  func infoPlist() -> Result<[String: Any], PlistReaderError>
  func plist(_ name: String) -> Result<[String: Any], PlistReaderError>
}
