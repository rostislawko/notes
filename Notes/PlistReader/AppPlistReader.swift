//
//  AppPlistReader.swift
//  Notes
//
//  Created by Rostislav Aliferovich on 7/4/19.
//

import Foundation

class AppPlistReader: PlistReader {
  func infoPlist() -> Result<[String: Any], PlistReaderError> {
    return plist("Info")
  }

  func plist(_ name: String) -> Result<[String: Any], PlistReaderError> {
    var plistFormat = PropertyListSerialization.PropertyListFormat.xml
    guard let plistPath = Bundle.main.path(forResource: name, ofType: "plist"),
          let plistData = FileManager.default.contents(atPath: plistPath),
          let plist = try? PropertyListSerialization.propertyList(
            from: plistData,
            options: .mutableContainersAndLeaves,
            format: &plistFormat
          ) as? [String: Any] else { return .failure(.read(name)) }
    return .success(plist)
  }
}
