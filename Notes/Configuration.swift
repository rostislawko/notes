//
//  Configuration.swift
//  Notes
//
//  Created by Rostislav Aliferovich on 7/4/19.
//

import Foundation

enum ConfigurationError: Error {
  case missingInfoPlist
  case variableNotFound(String)

  var description: String {
    switch self {
    case .missingInfoPlist: return "Configuration file is missing"
    case .variableNotFound(let key): return "\(key) parameter isn't defined in configuration file"
    }
  }
}

class Configuration {
  let baseUrl: String

  init(plistReader: PlistReader = AppPlistReader()) throws {
    switch plistReader.infoPlist() {
    case .success(let infoPlist):
      self.baseUrl = try Configuration.variable(plist: infoPlist, key: "BASE_URL")
    case .failure: throw ConfigurationError.missingInfoPlist
    }
  }

  private static func variable(plist: [String: Any], key: String) throws -> String {
    guard let variable = plist[key] as? String else {
      throw ConfigurationError.variableNotFound(key)
    }
    return variable
  }
}
