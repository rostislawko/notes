//
//  DateFormatter+CustomFormatters.swift
//  Notes
//
//  Created by Rostislav Aliferovich on 7/5/19.
//

import Foundation

extension DateFormatter {
  static let `default`: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    formatter.locale = Locale(identifier: "en_US_POSIX")
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    return formatter
  }()
}
