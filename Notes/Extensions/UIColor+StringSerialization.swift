//
//  UIColor+StringSerialization.swift
//  Notes
//
//  Created by Rostislav Aliferovich on 7/5/19.
//

import UIKit
import CoreImage

extension UIColor {
  var stringRepresentation: String {
    return CIColor(color: self).stringRepresentation
  }

  convenience init(stringRepresentation: String) {
    self.init(ciColor: CIColor(string: stringRepresentation))
  }
}
