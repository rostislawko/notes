//
//  FileNotebookTests.swift
//  NotesTests
//
//  Created by Rostislav Aliferovich on 7/5/19.
//

import XCTest

class FileNotebookTests: XCTestCase {
  func testFileErrorMessage() {
    XCTAssertEqual(FileError.read.description, "Could not read notes")
    XCTAssertEqual(FileError.write.description, "Could not save notes")
    XCTAssertEqual(FileError.createDirectory.description, "Can't create directory")
    XCTAssertEqual(FileError.unknown.description, "Unknown error")
  }

  func testAddNoteSucceededIfUidIsUnique() {
    let fileNotebook = FileNotebook()
    XCTAssertEqual(fileNotebook.notes.count, 0)
    fileNotebook.add(createTestNote(uid: "1"))
    XCTAssertEqual(fileNotebook.notes.count, 1)
  }

  func testAddNoteFailedIfUidIsNotUnique() {
    let fileNotebook = FileNotebook()
    XCTAssertEqual(fileNotebook.notes.count, 0)
    fileNotebook.add(createTestNote(uid: "1"))
    fileNotebook.add(createTestNote(uid: "1"))
    XCTAssertEqual(fileNotebook.notes.count, 1)
  }

  func testRemoveNoteSucceededIfUidFound() {
    let fileNotebook = FileNotebook()
    fileNotebook.add(createTestNote(uid: "1"))
    XCTAssertEqual(fileNotebook.notes.count, 1)
    fileNotebook.remove(with: "1")
    XCTAssertEqual(fileNotebook.notes.count, 0)
  }

  func testRemoveNoteFailedIfUidDoesNotFound() {
    let fileNotebook = FileNotebook()
    fileNotebook.add(createTestNote(uid: "1"))
    XCTAssertEqual(fileNotebook.notes.count, 1)
    fileNotebook.remove(with: "2")
    XCTAssertEqual(fileNotebook.notes.count, 1)
  }

  private func createTestNote(uid: String) -> Note {
    return Note(uid: uid, title: "testTitle", content: "testContent", importance: .low)
  }
}
