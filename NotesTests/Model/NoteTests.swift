//
//  NoteTests.swift
//  NotesTests
//
//  Created by Rostislav Aliferovich on 7/5/19.
//

import XCTest

class NoteTests: XCTestCase {
  func testNoteCreateWithDefaultValues() {
    let testTitle = "testTitle"
    let testContent = "testContent"
    let testImportance = Importance.high
    let note = Note(title: testTitle, content: testContent, importance: testImportance)
    XCTAssert(note.uid.isEmpty == false)
    XCTAssertEqual(note.title, testTitle)
    XCTAssertEqual(note.content, testContent)
    XCTAssertEqual(note.color, .white)
    XCTAssertEqual(note.importance, testImportance)
    XCTAssertNil(note.selfDestructionDate)
  }

  func testNoteCreateWithoutDefaultValues() {
    let testUid = "testUid"
    let testTitle = "testTitle"
    let testContent = "testContent"
    let testColor = UIColor.red
    let testImportance = Importance.high
    let testSelfDesctructionDate = Date(timeIntervalSince1970: 1000)
    let note = Note(
      uid: testUid,
      title: testTitle,
      content: testContent,
      color: testColor,
      importance: testImportance,
      selfDestructionDate: testSelfDesctructionDate
    )
    XCTAssertEqual(note.uid, testUid)
    XCTAssertEqual(note.title, testTitle)
    XCTAssertEqual(note.content, testContent)
    XCTAssertEqual(note.color, testColor)
    XCTAssertEqual(note.importance, testImportance)
    XCTAssertEqual(note.selfDestructionDate, testSelfDesctructionDate)
  }
}
