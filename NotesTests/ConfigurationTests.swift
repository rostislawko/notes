//
//  ConfigurationTests.swift
//  NotesTests
//
//  Created by Rostislav Aliferovich on 7/5/19.
//

import XCTest

class ConfigurationTests: XCTestCase {

  func testConfigurationCreationFailIfInfoPlistIsMissing() {
    let plistReader = PlistReaderSpy()
    plistReader.returnedPlist = .failure(.read("Info"))
    XCTAssertThrowsError(try Configuration(plistReader: plistReader)) { error in
      guard case ConfigurationError.missingInfoPlist = error else {
        return XCTFail("Mismatch returned error")
      }
    }
  }

  func testConfigurationCreationFailsIfBaseUrlIsMissing() {
    let plistReader = PlistReaderSpy()
    plistReader.returnedPlist = .success([String: Any]())
    XCTAssertThrowsError(try Configuration(plistReader: plistReader)) { error in
      guard case ConfigurationError.variableNotFound = error else {
        return XCTFail("Mismatch returned error")
      }
    }
  }

  func testConfigurationCreationSucceeded() {
    let plistReader = PlistReaderSpy()
    plistReader.returnedPlist = .success(["BASE_URL": "URL"])
    XCTAssertNoThrow(try Configuration(plistReader: plistReader))
  }

  func testBaseUrlIsCorrect() {
    let plistReader = PlistReaderSpy()
    plistReader.returnedPlist = .success(["BASE_URL": "URL"])
    let configuration = try! Configuration(plistReader: plistReader)
    XCTAssertEqual(configuration.baseUrl, "URL")
  }

  func testMissingInfoPlistErrorMessage() {
    XCTAssertEqual(ConfigurationError.missingInfoPlist.description, "Configuration file is missing")
  }

  func testVariableNotFoundErrorMessage() {
    let variableName = "TestVariable"
    XCTAssertEqual(
      ConfigurationError.variableNotFound(variableName).description,
      "TestVariable parameter isn't defined in configuration file"
    )
  }
}
