//
//  PlistReaderErrorTests.swift
//  NotesTests
//
//  Created by Rostislav Aliferovich on 7/5/19.
//

import XCTest

class PlistReaderErrorTests: XCTestCase {
  func testVariableNotFoundErrorMessage() {
    let plistName = "TestPlist"
    XCTAssertEqual(
      PlistReaderError.read(plistName).description,
      "Could not read TestPlist.plist"
    )
  }
}
