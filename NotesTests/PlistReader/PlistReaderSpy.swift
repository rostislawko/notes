//
//  PlistReaderSpy.swift
//  NotesTests
//
//  Created by Rostislav Aliferovich on 7/5/19.
//

import Foundation

class PlistReaderSpy: PlistReader {
  var returnedPlist: Result<[String: Any], PlistReaderError>?
  var passedName: String?

  func infoPlist() -> Result<[String : Any], PlistReaderError> {
    return returnedPlist!
  }

  func plist(_ name: String) -> Result<[String : Any], PlistReaderError> {
    passedName = name
    return returnedPlist!
  }
}
